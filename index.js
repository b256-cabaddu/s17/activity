function welcomeMessage() {
    let fullName = prompt("What is your name?");
    let age = prompt("How old are you?");
    let address = prompt("Where do you live?")
    
    alert("Thank you for your input.");

    console.log("Hello, " + fullName);
    console.log("You are " + age + "years old");
    console.log("You live in " + address);
}

welcomeMessage();

let top5FaveBands = function () {
    let faveBands = ["The Beatles", "Metallica", "The Eagles", "L\'arc\~en\~Ciel", "Eraserheads"]
    
    console.log("1. " + faveBands[0])
    console.log("2. " + faveBands[1])
    console.log("3. " + faveBands[2])
    console.log("4. " + faveBands[3])
    console.log("5. " + faveBands[4])
}

top5FaveBands()




function top5FaveMovs() {
    let faveMovs = {

        top1: {title: "The Godfather", rating: 97 + "\%"},

        top2: {title: "The Godfather Part II", rating: 96 + "\%"},

        top3: {title: "Shawshank Redemption", rating: 91 + "\%"},

        top4: {title: "To Kill a Mockingbird", rating: 93 + "\%"},

        top5: {title: "Psycho", rating: 96 + "\%"},
        
    }

    console.log("1. " + faveMovs.top1.title)
    console.log("Rotten Tomatoes Rating: " + faveMovs.top1.rating)

    console.log("2. " + faveMovs.top2.title)
    console.log("Rotten Tomatoes Rating: " + faveMovs.top2.rating)

    console.log("3. " + faveMovs.top3.title)
    console.log("Rotten Tomatoes Rating: " + faveMovs.top3.rating)

    console.log("4. " + faveMovs.top4.title)
    console.log("Rotten Tomatoes Rating: " + faveMovs.top4.rating)

    console.log("5. " + faveMovs.top5.title)
    console.log("Rotten Tomatoes Rating: " + faveMovs.top5.rating)
}

top5FaveMovs()



let printUsers = function() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();


